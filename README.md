# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
	This repository contains code written in python and go to demonstrate Binary search algorithm.
* Version
    1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
    Python => 2.7.x
	go => 1.7.x
* Configuration
    Python code ask for user input array,so the input should contains individual integers between space like ( 1 2 3 4).
	search key is defined inside the code via variable and can be changed.
* How to run tests
	python:
			create a instance from the binarysearch python class that should supplied with search key integer,and apply a search method on the instance.
			
			Array=binarysearch(array_input,5)
			print Array.search()
    Go:
	        Array and key are defined already,so run directly

### Who do I talk to? ###

* Repo owner or admin
	g.sabarinath91@gmail.com